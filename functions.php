<?php
// INCLUDES
require_once( get_stylesheet_directory(). '/src/scripts/yf-mail-encode.php' );
require_once( get_stylesheet_directory(). '/src/scripts/yf-shortcodes.php' );

add_shortcode( 'yf_encrypt_mail', 'shortcode_encrypt_mail' );
add_shortcode( 'yf_encrypt_phone', 'shortcode_encrypt_phone' );


// STANDARD SETTINGS
remove_filter ('the_content', 'wpautop'); // disables Wordpress to manually add tags, e.g. <p></p>

/*
 * Change google font
*/
add_filter( 'storefront_google_font_families', 'my_font_families' );
function my_font_families( $family ) {
    $family['opensans'] = 'Open+Sans:300,400,600';
    return $family;
}

/**
 * Add javascript includes
 * TODO: use task runner like GULP to bundle and minify multiple js-files for optimal inclusion
 */
function wpb_adding_scripts() {

    // TODO: find out how to use "$suffix" (empty)
    wp_register_script( 'yf_main_js', get_stylesheet_directory_uri() . '/yf-main.js', array(), '3.0.3', true );
    wp_enqueue_script('yf_main_js');
}
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );


/**
 * Remove product search
 */
function remove_sf_actions() {
    remove_action( 'storefront_header', 'storefront_header_cart', 60 );
    remove_action( 'storefront_header', 'storefront_product_search', 40 );

}
add_action( 'init', 'remove_sf_actions' );
add_action( 'storefront_header', 'storefront_header_cart',    29 ); // move cart up


/**
 * Remove product zoom
 */
function remove_pgz_theme_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'after_setup_theme', 'remove_pgz_theme_support', 100 );

/**
 * Change text in footer
 */
function storefront_credit() {
	
	?>
    	<div class="site-info" style="text-align: center;">
            <div class="yf-footer-icons-social">
                <a href="https://youtube.com/eyefactive" target="_blank" title="Folgen Sie uns auf YouTube">
                    <i class="yf-icon-social fab fa-youtube"></i>
                </a>
                <a href="https://facebook.com/eyefactive" target="_blank" title="Folgen Sie uns auf Facebook">
                    <i class="yf-icon-social fab fa-facebook"></i>
                </a>
                <a href="https://twitter.com/eyefactive" target="_blank" title="Folgen Sie uns auf Twitter">
                    <i class="yf-icon-social fab fa-twitter"></i>
                </a>
                <a href="https://plus.google.com/+eyefactive" target="_blank" title="Folgen Sie uns auf Google">
                    <i class="yf-icon-social fab fa-google-plus"></i>
                </a>
                <a href="http://www.linkedin.com/company/eyefactive-gmbh" target="_blank" title="Folgen Sie uns auf LinkedIn">
                    <i class="yf-icon-social fab fa-linkedin"></i>
                </a>
                <a href="http://pinterest.com/eyefactive" target="_blank" title="Folgen Sie uns auf Pinterest">
                    <i class="yf-icon-social fab fa-pinterest"></i>
                </a>
            </div>

            <div>
                <p>
                    <?php //echo __( 'Illustrations and contents are subject to alterations and errors.', 'woocommerce' ); ?>
                    Copyright <?=$date=date("Y")?>, <a href="https://www.eyefactive.com" target="_blank" title="www.eyefactive.com">eyefactive GmbH</a>.
                    <?php echo ( get_option( 'woocommerce_tax_display_shop' ) == 'incl' ) ? __( 'All prices incl. VAT.', 'woocommerce-germanized' ) : __( 'All prices excl. VAT.', 'woocommerce-germanized' ) ?>
                </p>
            </div>

        </div>
    <?php	
}


/**
 * Remove emoji support (strong performance boost)
 */
function remove_emoji() {
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	add_filter('tiny_mce_plugins', 'remove_tinymce_emoji');
}
add_action('init', 'remove_emoji');

function remove_tinymce_emoji($plugins) {
	if (!is_array($plugins)) {
		return array();
	}
	return array_diff($plugins, array('wpemoji'));
}


/**
 * USER REGISTRATION FORM: Customize form for b2b users
 */
function wooc_extra_register_fields() {
    ?>
    <p class="form-row form-row-first">
        <label for="reg_billing_first_name"><?php _e( 'Vorname', 'woocommerce' ); ?><span class="required"> *</span></label>
        <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
    <p class="form-row form-row-last">
        <label for="reg_billing_last_name"><?php _e( 'Nachname', 'woocommerce' ); ?><span class="required"> *</span></label>
        <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
    <p class="form-row form-row-first">
        <input type="radio" id="reg_account_type_company" name="account_type" value="company" checked>
        <label for="reg_account_type_company"class="inline"> Firmenkonto</label>
    </p>
    <p class="form-row form-row-last">
        <input type="radio" id="reg_account_type_private" name="account_type" value="private">
        <label for="reg_account_type_private" class="inline"> Privates Konto</label>
    </p>
    <p class="form-row form-row-wide" id="billing_company">
        <label for="reg_billing_company"><?php _e( 'Firma', 'woocommerce' ); ?><span class="required"> *</span></label>
        <input type="text" class="input-text" name="billing_company" id="reg_billing_company" value="<?php esc_attr_e( $_POST['billing_company'] ); ?>" />
    </p>

    <div class="clear"></div>
    <?php
 }
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );




/**
 * USER REGISTRATION FORM: Customize required fields in form
 */
function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $validation_errors->add( 'billing_first_name', __( 'Bitte geben Sie Ihren Vornamen ein.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $validation_errors->add( 'billing_last_name', __( 'Bitte geben Sie Ihren Nachnamen ein.', 'woocommerce' ) );
    }
    if ( ( $_POST['account_type'] == "company" ) && ( !$_POST['billing_company'] ) ) {
         $validation_errors->add( 'account_type_error', __( 'Bitte geben Sie einen Firmennamen ein.', 'woocommerce' ) );
    }

    return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );


/**
 * USER REGISTRATION FORM: Save extra form fields in database
 */
function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        //First name field which is by default
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        // First name field which is used in WooCommerce
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        // Last name field which is by default
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        // Last name field which is used in WooCommerce
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }
    if ( isset( $_POST['billing_company'] ) ) {
        // Company field which is by default
        update_user_meta( $customer_id, 'company', sanitize_text_field( $_POST['billing_company'] ) );
        // Company field which is used in WooCommerce
        update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
    }
    if ( isset( $_POST['account_type'] ) ) {
        // Account type field which is used in WooCommerce
        update_user_meta( $customer_id, 'account_type', sanitize_text_field( $_POST['account_type'] ) );
    }
    
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

/**
 * CHECKOUT FORM: Customize form fields: checkout
 */
function custom_override_checkout_fields( $fields ) {

    unset($fields['billing']['billing_address_2']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['billing']['billing_vat_id']);    // germanized
    unset($fields['shipping']['shipping_vat_id']);  // germanized
    unset($fields['billing']['billing_company']);

    //re-order form fields
    $fields['billing']['billing_first_name']['priority'] = 10;
    $fields['billing']['billing_last_name']['priority'] = 11;
    $fields['billing']['billing_email']['priority'] = 12;
    $fields['billing']['billing_phone']['priority'] = 13;
    //$fields['billing']['billing_company']['required'] = true;
    //$fields['billing']['billing_company']['priority'] = 20;
    //$fields['billing']['billing_vat']['priority'] = 21;
    $fields['billing']['billing_address_1']['priority'] = 30;
    //$fields['billing']['billing_address_2']['priority'] = 31;
    $fields['billing']['billing_postcode']['priority'] = 32;
    $fields['billing']['billing_city']['priority'] = 33;
    $fields['billing']['billing_country']['priority'] = 34;

    $fields['shipping']['shipping_address_1']['priority'] = 30;
    //$fields['shipping']['shipping_address_2']['priority'] = 31;
    $fields['shipping']['shipping_postcode']['priority'] = 32;
    $fields['shipping']['shipping_city']['priority'] = 33;
    $fields['shipping']['shipping_country']['priority'] = 34;

    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

/**
 * CHECKOUT FORM: Add radio buttons for account_type
 */
function custom_checkout_field($checkout){

    if (!is_user_logged_in()) {
            echo '<p class="form-row form-row-first">
                <input type="radio" id="reg_account_type_company" name="account_type" value="company" class="imradio" checked>
                <label for="reg_account_type_company"class="inline"> Firmenkonto</label>
            </p>
            <p class="form-row form-row-last">
                <input type="radio" id="reg_account_type_private" name="account_type" class="imradio" 
                value="private">
                <label for="reg_account_type_private" class="inline"> Privates Konto</label>
            </p> ';
    }

    echo '<div id="billing_company_container">
        <style>
            #billing_company_container > p{
                display:block!important;
            }
        </style>';

    woocommerce_form_field('billing_company', array(
        'type' => 'text',
        'required' => true,
        'class' => array(  'my-field-class form-row-wide' ),
        'label' => __('Firma') ,
    ) ,
        $checkout->get_value('billing_company'));

    woocommerce_form_field('billing_vat_id', array(
        'type' => 'text',
        'required' => false,
        'class' => array(  'my-field-class form-row-wide' ),
        'label' => __('Ust. ID') ,
        'placeholder'=>'DE.....'
    ),$checkout->get_value('billing_vat_id'));
   
    echo '</div>';

}

add_action('woocommerce_before_checkout_billing_form', 'custom_checkout_field');

function customise_checkout_field_process()
{
    if ($_POST['account_type'] == 'company') {
        if (!$_POST['billing_company']) wc_add_notice(__('<strong>Rechnung Firma</strong> ist ein Pflichtfeld..') , 'error');
    }
}

add_action('woocommerce_checkout_process', 'customise_checkout_field_process');
// update billing_vat_id
function my_custom_checkout_field_update_user_meta( $user_id ) {

    if ($user_id && $_POST['billing_vat_id']){
        update_user_meta( $user_id, 'billing_vat_id', esc_attr($_POST['billing_vat_id']) );
    }
    if (!$_POST['ship_to_different_address']) {
        if ($user_id && $_POST['billing_company']) {
            update_user_meta( $user_id, 'shipping_company', esc_attr($_POST['billing_company']) );
        }
    }
}
add_action('woocommerce_checkout_update_user_meta', 'my_custom_checkout_field_update_user_meta');


function wooc_add_home_icon($links) {
    $new_links = array(
        'home' => array(
            'priority' => 10,
            'callback' => 'storefront_handheld_footer_bar_home_link',
        ),
    );
    $links = array_merge( $new_links, $links );
    return $links;
}

if ( ! function_exists( 'storefront_handheld_footer_bar_home_link' ) ) {
    /**
     * The account callback function for the handheld footer bar
     *
     * @since 2.0.0
     */
    function storefront_handheld_footer_bar_home_link() {
        ?>
            <a class="glyphicon glyphicon-home" href="<?php echo esc_url( get_home_url() ); ?>" title="<?php esc_attr_e( 'Home', 'storefront' ); ?>" ></a>
        <?php
    }
}
add_filter( 'storefront_handheld_footer_bar_links', 'wooc_add_home_icon' );




// Delete billing_address_2 in billing
function wc_npr_filter_postcode( $address_fields ) {
    $address_fields['billing_address_2']['type'] = 'hidden';
    return $address_fields;
}
add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_postcode');

// Delete shipping_address_2,shipping_vat_id in shipping
function remove_shipping_field($fields) {
    unset( $fields ['shipping_address_2'] );
    unset( $fields ['shipping_vat_id'] );
    return $fields;
}
add_filter( 'woocommerce_shipping_fields', 'remove_shipping_field', 20, 1 );

// Change billing_vat to billing_vat_id
function action_woocommerce_after_save_address_validation( $user_id, $load_address, $address ) {
    if ($_POST['billing_vat']) {
        $_POST['billing_vat_id'] = $_POST['billing_vat'] ;
        unset($_POST['billing_vat']);
        update_user_meta( $user_id, 'billing_vat_id', esc_attr($_POST['billing_vat_id']) );
    }
}
add_action( 'woocommerce_after_save_address_validation', 'action_woocommerce_after_save_address_validation', 10, 3 );

/**
 * Display the footer widget regions. (override cols to 3!)
 *
 */
function delay_remove() {
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
}
add_action('init','delay_remove');




/**
 * Display the footer widget regions. (override cols to 3!)
 *
 * @since  1.0.0
 * @return void
 */
//if ( ! function_exists( 'storefront_footer_widgets' ) ) {
function storefront_footer_widgets() {
    $rows    = intval( apply_filters( 'storefront_footer_widget_rows', 1 ) );
    $regions = intval( apply_filters( 'storefront_footer_widget_columns', 3 ) );

    for ( $row = 1; $row <= $rows; $row++ ) :

        // Defines the number of active columns in this footer row.
        for ( $region = $regions; 0 < $region; $region-- ) {
            if ( is_active_sidebar( 'footer-' . strval( $region + $regions * ( $row - 1 ) ) ) ) {
                $columns = $region;
                break;
            }
        }

        if ( isset( $columns ) ) : ?>
            <div class=<?php echo '"footer-widgets row-' . strval( $row ) . ' col-' . strval( $columns ) . ' fix"'; ?>><?php

                for ( $column = 1; $column <= $columns; $column++ ) :
                    $footer_n = $column + $regions * ( $row - 1 );

                    if ( is_active_sidebar( 'footer-' . strval( $footer_n ) ) ) : ?>

                    <div class="block footer-widget-<?php echo strval( $column ); ?>">
                        <?php dynamic_sidebar( 'footer-' . strval( $footer_n ) ); ?>
                        </div><?php

                    endif;
                endfor; ?>

            </div><!-- .footer-widgets.row-<?php echo strval( $row ); ?> --><?php

            unset( $columns );
        endif;
    endfor;
}
//}



/**
 * Enable PHP in widgets
 */
function enable_php_in_widgets($text) {
    if (strpos($text, '<' . '?') !== false) {
        ob_start();
        eval('?' . '>' . $text);
        $text = ob_get_contents();
        ob_end_clean();
    }
    return $text;
}
add_filter('widget_text', 'enable_php_in_widgets', 99);

/**
 * Remove footer links in mobile footer
 *
 * @since  1.0.0
 * @return void
 */
function yf_remove_handheld_footer_links( $links ) {
    //unset( $links['my-account'] );
    unset( $links['search'] );
    //unset( $links['cart'] );

    return $links;
}
add_filter( 'storefront_handheld_footer_bar_links', 'yf_remove_handheld_footer_links' );


/**
 * @snippet       Remove "Showing the Single Result" Storefront Theme - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @source        https://businessbloomer.com/?p=369
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.3.1
 */
function delay_remove_result_count() {
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
}
add_action('init','delay_remove_result_count');

/**
 * Load translation files from your child theme instead of the parent theme
 *
 * https://docs.woocommerce.com/document/woocommerce-localization/
 * Replace 'textdomain' with your plugin's textdomain. e.g. 'woocommerce'.
 * File to be named, for example, yourtranslationfile-en_GB.mo
 * File to be placed, for example, wp-content/lanaguages/textdomain/yourtranslationfile-en_GB.mo
 *
 */
function load_custom_plugin_translation_file( $mofile, $domain ) {
    if ( 'woocommerce' === $domain ) {
        $mofile = get_stylesheet_directory() . '/languages/woocommerce-de_DE_formal.mo';
        //$mofile = WP_LANG_DIR . '/textdomain/yourtranslationfile-' . get_locale() . '.mo';
    }
    return $mofile;
}
add_filter( 'load_textdomain_mofile', 'load_custom_plugin_translation_file', 10, 2 );

function wpb_woo_my_account_order() {
    $myorder = array(
        //'my-custom-endpoint' => __( 'My Stuff', 'woocommerce' ),
        'dashboard'          => __( 'Dashboard', 'woocommerce' ),
        'orders'             => __( 'Orders', 'woocommerce' ),
        //'downloads'          => __( 'Download MP4s', 'woocommerce' ),
        //'edit-account'       => __( 'Account details', 'woocommerce' ),
        'edit-address'       => __( 'Addresses', 'woocommerce' ),
        //'payment-methods'    => __( 'Payment Methods', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),
    );
    return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );

/*
 * Change the entry title of the endpoints that appear in My Account Page - WooCommerce 2.6
 * Using the_title filter
 *
 * https://wpbeaches.com/change-rename-woocommerce-endpoints-accounts-page/
 */
function wpb_woo_endpoint_title( $title, $id ) {
    if ( is_wc_endpoint_url( 'orders' ) && in_the_loop() ) { // add your endpoint urls
        $title = "Bestellungen"; // change your entry-title
    }
    return $title;

}
add_filter( 'the_title', 'wpb_woo_endpoint_title', 10, 2 );

/*Super WP Heroes */

remove_action( 'woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_cart_total_tax', 1 );
add_action( 'woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_cart_total_tax_overwrite', 1 );


function woocommerce_gzd_template_cart_total_tax_overwrite() {

    if(function_exists('wc_gzd_get_cart_total_taxes') && function_exists('wc_gzd_get_tax_rate_label')){
        foreach ( wc_gzd_get_cart_total_taxes() as $tax ) :

            $label = wc_gzd_get_tax_rate_label( $tax[ 'tax' ]->rate );

        ?>
            <tr class="order-tax">
                <th colspan="2"><?php echo $label; ?></th>
                <td colspan="2" data-title="<?php echo esc_attr( $label ); ?>"><?php echo wc_price( $tax[ 'amount' ] ); ?></td>
            </tr>

        <?php endforeach;
    }
}

/*
 * Show wide branding on front page ("home")
 */
function my_theme_wrapper_start() {
    if (is_front_page()) {
        ?>
        <div class="yf-header-home">

            <div class="yf-layout-stacked">
                <div class="yf-container-stacked">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/touchscreen-store-branding-01.jpg" />
                </div>

                <div class="yf-container-stacked yf-centered-vertical">
                    <div class="col-full">
                        <div class="yf-content-stage">
                            <!--<span>www.touchscreen-store.com</span>-->
                            <h1>Online Shop für MultiTouch Screens, Systeme & Zubehör</h1>
                            <p>Einfach, schnell & bequem online bestellen: Interaktive MultiTouch Screens, Tische, Kiosk-Terminals in unterschiedlichen Größen und Formaten kaufen. Individuelles Zubehör wie innovative Touchscreen Objekterkennung, Mediaplayer, Standfüße und vieles mehr!</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="yf-reference-logos">
                <div class="col-full">
                    <div>
                        <img class="yf-img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/touchscreen-store-reference-logos-01.jpg" alt="Logos A" title="Logos A1">
                    </div>
                    <div>
                        <img class="yf-img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/touchscreen-store-reference-logos-02.jpg" alt="Logos B" title="Logos B1">
                    </div>
                </div>
            </div>

        </div>

        <?php
    }
}
add_action('storefront_before_content', 'my_theme_wrapper_start', 10);


/*
 * Show wide branding on front page ("home")
 */
function my_theme_wrapper_end()
{
    if (is_front_page()) {
        ?>
        <!-- CARD: DISCOUNT -->
        <div class="yf-space-bottom-std">
            <div class="col-full">
                <div class="yf-card yf-card-horizontal">
                    <div class="yf-img">
                        <img src="wp-content/themes/storefront-child/img/home/discount-banner-02.jpg" />
                    </div>
                    <div class="yf-content yf-bg-yfgreen yf-invert">
                        <span>Nur für kurze Zeit!</span>
                        <h2>Rabatt-Aktion für MultiTouch Apps</h2>
                        <p><!--Vielen Dank für Ihren Besuch auf www.touchscreen-store.com. --><strong>Allen Neukunden gewähren wir einen Rabatt von 50% auf eine beliebige Touchscreen App</strong> aus dem eyefactive App Store (www.multitouch-appstore.com). Die perfekte Ergänzung zu Ihrer neuen Touchscreen Hardware!</p>
                        <p>Einen entsprechenden Gutschein-Code senden wir Ihnen automatisch nach Ihrer ersten Bestellung zu.</p>
                        <!--<span class="yf-label-button yf-glass">TSSAPP50</span>-->
                    </div>
                </div>
            </div>
        </div>


        <!-- CARD: CONTACT -->
        <div class="yf-space-bottom-std">
            <div class="col-full">

                <div class="yf-card yf-card-horizontal">
                    <div class="yf-content">
                        <h2>Kontakt</h2>
                        <p>Wir beraten Sie gerne kostenlos und unverbindlich. Schreiben Sie uns einfach eine Nachricht oder rufen Sie uns an:</p>
                        <span class="yf-label-button"><i class="fas fa-phone-volume" aria-hidden="true"></i> <?php echo telefonnummer('+49 4103 / 90380 0'); ?></span>
                        <span class="button"><i class="far fa-envelope" aria-hidden="true"></i> <?php echo mailLink('info@touchscreen-store.com', 'maillink'); ?></span>
                    </div>
                    <div class="yf-img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/contact.jpg" />
                    </div>
                </div>

            </div>
        </div>

        <!-- BENEFITS -->
        <div class="yf-space-top-100 yf-bg-linear-dg-yfviolet yf-invert yf-centered">
            <div class="col-full">
                <h2>Vielfältige Einsatzmöglichkeiten</h2>
                <p>Erschaffen Sie spannende Interactive Digital Signage Lösungen für Ihre Kunden am Point of Sale, Information und Entertainment. Nutzen Sie große, professionelle Multi Touch Screens, Tische, Infoterminals, Schaufenster und Videowände für effektives Marketing, spannendes Infotainment und kollaboratives Teamwork.</p>


                <ul class="yf-col3-set yf-bullets-center yf-icon-white yf-text-white">
                    <li class="yf-col-1">
                        <span><i class="fas fa-gift" aria-hidden="true"></i></span>
                        <p>Point of Sale</p>
                        <h4>Mehr verkaufen</h4>
                    </li>
                    <li class="yf-col-2">
                        <span><i class="fas fa-info-circle" aria-hidden="true"></i></span>
                        <p>Point of Information</p>
                        <h4>Individueller informieren</h4>
                    </li>
                    <li class="yf-col-3">
                        <span><i class="fas fa-gamepad" aria-hidden="true"></i></span>
                        <p>Point of Entertainment</p>
                        <h4>Spannender unterhalten</h4>
                    </li>
                    <li class="yf-col-1">
                        <span><i class="fas fa-university" aria-hidden="true"></i></span>
                        <p>Point of Education</p>
                        <h4>Wissen nachhaltiger transportieren</h4>
                    </li>
                    <li class="yf-col-2">
                        <span><i class="fas fa-map-marker-alt" aria-hidden="true"></i></span>
                        <p>Point of Advertisement</p>
                        <h4>Effektiver werben</h4>
                    </li>
                    <li class="yf-col-3">
                        <span><i class="fa fa-users" aria-hidden="true"></i></span>
                        <p>Point of Collaboration</p>
                        <h4>Kreativer &amp; effizienter arbeiten</h4>
                    </li>
                </ul>

            </div>
        </div>

        <?php
    }
}
add_action('storefront_before_footer', 'my_theme_wrapper_end', 10);

/*
 * Disable shop page
 */
function woocommerce_disable_shop_page()
{
    global $post;
    if (is_shop()):
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
    endif;
}
add_action( 'wp', 'woocommerce_disable_shop_page' );

/*
 * Change breadcrumbs
 */
function custom_breadcrumb( $crumbs, $object_class )
{
    // Loop through all $crumb
    foreach( $crumbs as $key => $crumb ){
        $taxonomy = 'product_cat'; // The product category taxonomy

        // Check if it is a product category term
        $term_array = term_exists( $crumb[0], $taxonomy );

        // if it is a product category term
        if ( $term_array !== 0 && $term_array !== null ) {

            // Get the WP_Term instance object
            $term = get_term( $term_array['term_id'], $taxonomy );

            //echo "<pre>"; var_dump($term); echo "</pre>";

            switch ($term->slug) {
                case 'multitouch-screens':
                case 'multitouch-systeme':
                case 'touchscreen-zubehoer':
                    // HERE set your new link with a custom one
                    $crumbs[$key][1] = home_url( '/'.$term->slug.'/' ); // or use all other dedicated functions
                    break;
            }
        }
    }

    return $crumbs;
}
add_filter( 'woocommerce_get_breadcrumb', 'custom_breadcrumb', 10, 2 );

/*
 * WP Heroes: Custom order alert messages
 */
function swph_show_thank_you_message($order) {
    if($order->is_paid()) {
        echo '<div class="woocommerce-info" style="display:block !important;">' . __( 'Thank you for your order, payment was received.', 'woocommerce') . '</div>';
    }
    else {
        echo '<div class="woocommerce-info" style="display:block !important;">' . __( 'Thank you for your order, shipping will be processed after payment comfirmation.', 'woocommerce') . '</div>';
    }
}


/**
 * WP Heroes: Display field account type on the order edit page
 */
function add_company_type_field_display_admin_order_meta($order){

    $order_user_id = $order->get_user_id();
    $order_user_type = get_user_meta($order_user_id,'account_type', true);

    echo '<p><strong>'.__('Account Type').':</strong> <br/>' . $order_user_type . '</p>';
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'add_company_type_field_display_admin_order_meta', 10, 1 );


/**
 * WP Heroes: Display field account type on the users table - admin view - /wp-admin/users.php
 */
function swph_modify_user_table( $columns ) {
    /* Unset columns in users table to be able to modify the order of those */
    unset($columns);

    /* Modify the order of the columns on users table */
    $columns['cb'] = '<input type="checkbox">';
    $columns['username'] = __('Username');
    $columns['name'] = __('Name');
    $columns['account_type'] = __('Account Type');
    $columns['email'] = __('Email');
    $columns['role'] = __('Role');
    $columns['posts'] = __('Posts');
    return $columns;
}
add_filter( 'manage_users_columns', 'swph_modify_user_table' , 10,1);


/**
 * WP Heroes: Populate account_type columns in <tr> - admin view - /wp-admin/users.php
 */

function swph_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'account_type' :
            return get_user_meta($user_id, 'account_type', true);
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'swph_modify_user_table_row', 10, 3 );



/**
 * WP Heroes: Display custom user meta: account_type in single user admin view wp-admin/user-edit.php?
 */
function swph_custom_user_profile_fields($user){
    $account_type = '';
    if( is_object($user) && isset($user->ID) ) {
        $account_type = get_user_meta( $user->ID, 'account_type', true );
    }
    ?>
    <h3>Account Type</h3>
    <table class="form-table">
        <tr>
            <th><label for="company">Account Type</label></th>
            <td><?php echo $account_type ; ?></td>
        </tr>
    </table>
<?php
}
add_action( 'edit_user_profile', 'swph_custom_user_profile_fields', 1, 1 );

